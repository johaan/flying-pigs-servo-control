parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

client_path="$parent_path/client"
server_path="$parent_path/artnet-servo-api"

xcode-select --install
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash

source ~/.nvm/nvm.sh

nvm install v12.3.1
nvm use v12.3.1
npm install yarn -g

cd $client_path
yarn install
yarn build

cd $server_path
yarn install