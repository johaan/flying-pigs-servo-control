parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

server_path="$parent_path/artnet-servo-api"

source ~/.nvm/nvm.sh

nvm use v12.3.1

cd $server_path
yarn start