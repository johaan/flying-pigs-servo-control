

# Installation
$ git clone https://gitlab.com/johaan/flying-pigs-servo-control.git

$ flying-pigs-servo-control/install.sh

# Start the server
$ flying-pigs-servo-control/start.sh

Access the control interface at http://127.0.0.1:5000