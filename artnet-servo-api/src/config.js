
module.exports = {
  DMX_MAX_VALUE: 255,
  DEFAULTS: {
    ARTNET_IP: '169.254.60.2',
    OSC_LISTEN_PORT: 8000,
    OSC_FORWARD_PORT: 1234,
  }
  
}