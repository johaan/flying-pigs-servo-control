
// OSC
const OSC = require('osc-js')

module.exports = class OscController {

  constructor(db, mc) {
    this.db = db
    this.mc = mc
    
    this.osc = new OSC({ plugin: new OSC.DatagramPlugin() })
    this.open()
  }

  open() {

    if(this.osc.status()) {
      this.osc.close()
      this.osc = new OSC({ plugin: new OSC.DatagramPlugin() })
    }

    this.osc.on('/motors/*', message => {
      this.mc.handleOSC(message, this.osc)
    })
    
    this.osc.on('open', () => {
      console.log("osc open")
    })
    
    this.osc.on('close', () => {
      console.log("osc close")
    });
      
    this.osc.on('error', (err) => {
      console.log("OSC Error: ", err)
    });
  
    this.osc.open({ host: '0.0.0.0', port: this.db.get('settings.osc_listen_port').value() })
  }

  close() {
    if(this.osc.status()) {
      this.osc.close()
    }
  }

}