const {DEFAULTS} = require('./config')
const path = require('path')
const fs = require('fs')

// Database
const low = require('lowdb')
//const FileAsync = require('lowdb/adapters/FileAsync')
const FileSync = require('lowdb/adapters/FileSync')

// create data directory if it does not exists:
const db_dir = path.join(__dirname, '../../data/')
if (!fs.existsSync(db_dir)){
  fs.mkdirSync(db_dir);
}

const adapter = new FileSync(path.join(db_dir, 'db.json'))

const db = low(adapter)
db.defaults(
  {
    motors: [],
    settings: {
      osc_forward_enabled: true,
      osc_forward_host: 'localhost',
      osc_forward_port: DEFAULTS.OSC_FORWARD_PORT,
      artnet_ip: DEFAULTS.ARTNET_IP,
      osc_listen_port: DEFAULTS.OSC_LISTEN_PORT,
    }
  }
  ).write()

const MotorController = require('./motor-controller')
var mc = new MotorController(db)

const OscController = require('./osc-controller')
const oscController = new OscController(db, mc)

const fastify = require('fastify')({ logger: true })
fastify.register(require('fastify-cors'), {})

fastify.register(require('fastify-static'), {
  root: path.join(__dirname, '../../client/build'),
  //prefix: '/public/', // optional: default '/'
})

fastify.register(require('./routes'), {db: db, mc: mc, osc: oscController})

const startFastify = async () => {
  try {
    await fastify.listen(5000, "0.0.0.0")
    fastify.log.info(`server listening on ${fastify.server.address().port}`)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}

startFastify()