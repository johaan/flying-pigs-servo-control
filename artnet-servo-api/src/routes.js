const { NotFound } = require('http-errors')

const MotorSchema = {
  $id: 'motor',
  type: 'object',
  properties: {
    id: { type: 'string'},
    name: { type: 'string'},
    // type: { type: 'string' }, // TODO: enum validate
    value: { type: 'number' }, // TODO: 0 to 1
    min_value: { type: 'number' }, // TODO: 0 to 1 and lower tah max
    max_value: { type: 'number' }, // TODO: 0 to 1 and higher than min
    channel: { "anyOf": [
      {type: "integer"}, // TODO: 1 to 512,
      {type: "null"}
      ], 
    },
    osc_forward: { type: 'boolean' },
    enabled: { type: 'boolean' },
  }
}

const SettingsSchema = {
  $id: 'settings',
  type: 'object',
  properties: {
    osc_forward_enabled: {type: 'boolean'},
    osc_forward_host: {type: 'string'},
    osc_forward_port: {type: 'integer'},
    osc_listen_port: { type: 'integer' },
    artnet_ip: { type: 'string' },
  }
}

module.exports = function (fastify, opts, done) {

  const db = opts.db;
  const mc = opts.mc;
  const osc = opts.osc;

  fastify.addSchema(SettingsSchema)
  fastify.addSchema(MotorSchema)

  const updateSettings = function(settings) {

    const current_settings = db.get('settings', settings).value()

    db.set('settings', settings).write()
    
    if(settings.osc_listen_port !== current_settings.osc_listen_port) {
      osc.open()
    }
    if(settings.artnet_ip !== current_settings.artnet_ip) {
      mc.connectArtnet()
    }
  }

  fastify.get('/',function (req, reply) {
    reply.sendFile('index.html')
  })

  fastify.route({
    method: 'GET',
    url: '/api/motors',
    querystring: {
    },
    schema: {
      response: {
        200: { 
          type: 'array',
          items: 'motor#'
        }
      }
    },
    handler: async (request, reply) => {
        reply.send(mc.getMotors())
    }
  })
  
  
  fastify.route({
    method: 'GET',
    url: '/api/state',
    schema: {
      response: {
        200: { 
          type: 'object',
          properties: {
            motors: {
              type: 'array',
              items: 'motor#'
            },
            settings: 'settings#'
          }
        }
      }
    },
    preHandler: async (request, reply) => {
    },
    handler: async (request, reply) => {
      reply.send(db.getState())
    }
  })
  
  fastify.route({
    method: 'GET',
    url: '/api/settings',
    schema: {
      response: {
        200: 'settings#'
      }
    },
    preHandler: async (request, reply) => {
    },
    handler: async (request, reply) => {
      reply.send(db.get('settings').value())
    }
  })
  
  fastify.route({
    method: 'PUT',
    url: '/api/settings',
    schema: {
      body: 'settings#',
      response: {
        200: 'settings#'
      }
    },
    preHandler: async (request, reply) => {
    },
    handler: async (request, reply) => {
      updateSettings(request.body)
      reply.send(db.get('settings').value())
    }
  })
  
  fastify.route({
    method: 'PUT',
    url: '/api/state',
    schema: {
      body: {
        type: 'object',
        properties: {
          motors: {
            type: 'array',
            items: 'motor#'
          },
          settings: 'settings#'
        }
      },
      response: {
        200: { 
          type: 'object',
          properties: {
            motors: {
              type: 'array',
              items: 'motor#'
            },
            settings: 'settings#'
          }
        }
      }
    },
    handler: async (request, reply) => {
      console.log("body: ", request.body)
      db.set('motors', request.body.motors).write()

      updateSettings(request.body.settings)

      reply.send(db.getState())
    }
  })
  
  fastify.route({
    method: 'POST',
    url: '/api/motors',
    schema: {
      body: 'motor#',
      response: {
        201: 'motor#'
      }
    },
    handler: async (request, reply) => {
      const motor = mc.addMotor()
      reply.code(201).send(motor)
    }
  })
  
  fastify.route({
    method: 'GET',
    url: '/api/motors/:id',
    schema: {
      response: {
        200: 'motor#'
      }
    },
    handler: async (request, reply) => {
      const motor = mc.getMotor(request.params.id)
      if(!motor) throw new NotFound()

      reply.send(motor)
    }
  })
  
  fastify.route({
    method: 'PUT',
    url: '/api/motors/:id',
    schema: {
      body: 'motor#',
      response: {
        200: 'motor#'
      }
    },
    handler: async (request, reply) => {
      motor = mc.updateMotor(request.params.id, request.body)
      if(!motor) throw new NotFound()
      reply.send(motor)
    }
  })
  
  fastify.route({
    method: 'DELETE',
    url: '/api/motors/:id',
    schema: {
      response: {
        204: {type: 'null'}
      }
    },
    handler: async (request, reply) => {
      if(!mc.getMotor(request.params.id)) throw new NotFound()
      
      mc.deleteMotor(request.params.id)
      reply.code(204).send()
    }
  })

  done()
}

