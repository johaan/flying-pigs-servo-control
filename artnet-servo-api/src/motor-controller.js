// DMX - artnet
const DMX = require('dmx')

const shortid = require('shortid')
const OSC = require('osc-js')
const { DMX_MAX_VALUE } = require('./config')

const map = function(value, istart, istop, ostart, ostop) {
  return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
}

module.exports = class MotorController {
  constructor(db) {
    this.db = db

    this.connectArtnet()
  }

  connectArtnet() {
    this.dmx = new DMX() 
    this.servoArtnet = this.dmx.addUniverse('servo-artnet', 'artnet', this.db.get("settings.artnet_ip").value() );
  }

  handleOSC(message, osc) {
    
    const name = message.address.split('/motors/')[1]
    const m = this.getMotorByName(name)

    if(m) {
      this.updateMotor(m.id, 
        {value: message.args[0]}
      )
    }

    if(this.db.get("settings.osc_forward_enabled").value() && m.osc_forward) {

      const forward_message = new OSC.Message(message.address, message.args[0]);
      if(message.args[0] >= 1) {
        //console.log(forward_message)
        // TODO: vezer has a bug, if it listens to a 1 value it maps down to 0 - would be a bt dirty to fix it here but possible
      }
      osc.send(forward_message, { 
          host: this.db.get("settings.osc_forward_host").value(), 
          port: this.db.get("settings.osc_forward_port").value() 
        })
    }

  }

  motors(){
    return this.db.get('motors')
  }

  motor(id) {
    return this.motors().find({ id: id })
  }

  getMotor(id) {
    return this.motor(id).value()
  }

  getMotorByName(name) {
    return this.motors().find({ name: name }).value()
  }

  getMotors() { return this.motors().value() }

  getMotorCount() {
    return this.motors().size().value()
  }

  deleteMotor(id) {
    return this.motors()
    .remove({ id: id })
    .write()
  }

  addMotor() {
    let id = shortid.generate()

    return this.motors()
      .push({ id: id,
        name: this.getUniqueMotorName(), 
        //type: 'servo', 
        channel: null, 
        value: 0.5, 
        min_value: 0,
        max_value: 1,
        osc_forward: true,
        enabled: false
        })
      .find({ id: id }).write()
  }

  getUniqueMotorName = (name=null) => {
    const tn = (_n) => {
      let _sn = _n.join("")
      if (!this.getMotorByName(_sn)) {
        return _sn
      }

      if(_n.length < 2) {
        _n.push(0)
      }
      _n[1]++
      return tn(_n)
    }
    
    let n = (name === null) ? ["M", this.getMotorCount()+1] : [name.replace('/[\W]+/g',"")]
    return tn(n)
  }

  updateMotor(id, data) {
    delete data.id

    //console.log(data)
    if(data.name) {
      data.name = this.getUniqueMotorName(data.name)
      //console.log(this.getMotorByName(data.name))
      //console.log(data.name)
    }

    const m = this.motor(id)
      .assign(data)
      .write()

    if (m.enabled) {
      this.writeArtnet(m.channel, map(m.value, 0, 1, m.min_value, m.max_value))
    }
    return m
  }

  /*updatePosition(id, value) {
    let m = this.motor(id)
      .assign({ value: value})
      .write()
    //this.servoArtnet.update({1: 0, 2: 80});

    this.writeArtnet(m.channel, m.value)
  }*/

  writeArtnet(channel, position) {
    let o = {}; o[channel] = position * DMX_MAX_VALUE
    //console.log("write motor: ", o)
    this.servoArtnet.update(o);
  }

}