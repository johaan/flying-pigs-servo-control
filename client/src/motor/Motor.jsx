import React, { Component, Fragment } from 'react'
import MotorRangeSlider from './RangeSlider';
import MotorTypeSelector from './TypeSelector';
import MotorNameField from './NameField';
import MotorDeleteButton from './DeleteButton';

import ChannelField from './ChannelField';
import { withStyles } from '@material-ui/core/styles';
import Fade from '@material-ui/core/Fade';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import Switch from '@material-ui/core/Switch';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const styles = theme => ({
  container: {
    position: 'relative',
    padding: theme.spacing.unit * 2,
  }
});

class Motor extends Component{

  constructor(props) {
    super(props);
    this.state = {
      show: true,
      //osc_forward: props.motor.osc_forward,
      //enabled: props.motor.enabled
    };
  }

  componentWillUnmount() {
  }

  static getDerivedStateFromProps(props, state) {
    if (props.motor.deleted === true) {
      return {deleted: true}
    } 
    return null
  }

  /*handleTypeChange = (_type) => {
    console.log("type change", _type)
    this.props.API.updateConfig(this.props.motor, {"type": _type})
  }*/

  handleNameChange = (_name) => {
    console.log("handleNameChange")
    this.props.API.updateConfig(this.props.motor, {"name": _name})
  }

  handleDelete = async (e) => {
    console.log("handleDelete")
    const del = await this.props.API.delete(this.props.motor)
    console.log("del", del)
    if(del) {
      this.setState({
        deleted: true
      })
    }
  }

  handleRangeChange = (_range) => {
    //console.log("handleRangeChange")
    this.props.API.updateConfig(this.props.motor, {min_value: _range[0], max_value: _range[1]})
  }

  handleValueChange = (_v) => {
    //console.log("handleValueChange")
    const new_value = _v[0];
    this.props.API.updateConfig(this.props.motor, {value: parseFloat(new_value)})
    //TODO: this.props.API.updateValue(this.props.motor, parseFloat(new_value))
  }

  handleChannelChange = (channel) => {
    console.log("handleChannelChange")
    this.props.API.updateConfig(this.props.motor, {channel: parseInt(channel)})
  }

  toggleOSCForward = (event) => {
    this.props.API.updateConfig(this.props.motor, {osc_forward: event.target.checked})
  }

  toggleEnabled = (event) => {
    this.props.API.updateConfig(this.props.motor, {enabled: event.target.checked})
  }

  render() {
    const { classes, editMode } = this.props;

    const {
      name,
      //type,
      value,
      min_value,
      max_value,
      channel,

    } = this.props.motor;

    const { deleted } = this.state
    const range = [min_value, max_value]
    
    return (
      <Grow in={!deleted} unmountOnExit>
      <Grid item xs={6} sm={3} md={2} xl={1}>
      <Paper className={classes.container}>

      <Grid container spacing={0}>

        <Grid item xs={2}>
        <MotorRangeSlider handleSliderChange={this.handleValueChange} values={[value]}/>
        </Grid>

        <Grid item xs={2}>
        <MotorRangeSlider handleSliderChange={this.handleRangeChange} values={range} disabled={false} reversed={true}/>
        </Grid>

        <Grid item xs={8}>
        <FormGroup>

          <MotorNameField 
            value={name} 
            handleNameChange={this.handleNameChange} />

          {/*<MotorTypeSelector 
            value={type} 
          handleTypeChange={this.handleTypeChange} />*/}

          <ChannelField
            value={channel || ''}
            handleChannelChange={this.handleChannelChange} />

          <FormControlLabel
            control={<Switch size="small" checked={this.props.motor.osc_forward} onChange={this.toggleOSCForward} />}
            label="OSC Forward"
          />

          <FormControlLabel
            control={<Switch size="small" checked={this.props.motor.enabled} onChange={this.toggleEnabled} />}
            label="Enable"
          />
        </FormGroup>
        
        </Grid>
      </Grid>

        <div>
        <MotorDeleteButton handleDelete={this.handleDelete} />
        </div>

      </Paper>
      </Grid>
      </Grow>
    );
  }
}

export default withStyles(styles)(Motor);