
import React, { Component } from 'react'

import Motor from './Motor'
import SettingsDialog from './SettingsDialog';

import PropTypes from 'prop-types';

import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';

import Fade from '@material-ui/core/Fade';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Drawer from '@material-ui/core/Drawer';

import CircularProgress from '@material-ui/core/CircularProgress';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import _ from 'lodash';

const theme = createMuiTheme({

  palette: {
    type: 'dark',
    primary: {
      main: '#FFD300'
    },
    motorValueColor: '#FFD300',
    motorRangeColor: '#00A86B'
  },

  typography: {
    fontSize: 10,
    useNextVariants: true,
  },
});

const styles = theme => ({
  root: {
    flexGrow: 1,
  },

  add_button: {
    position: 'absolute', 
    top: '50%',
    left: '50%', 
    transform: 'translate(-50%, -50%)'
  },

  motor_list: {
    marginBottom: theme.spacing.unit * 9,
  },

  appBar: {
    top: 'auto',
    bottom: 0,
  },

  toolbar: {
    position: 'relative'
  },

  menuButton: {
    marginRight: theme.spacing.unit * 2,
  },

  title: {
    flexGrow: 1,
  },

  add_container: {
    position: 'relative',
    height: 248,
    padding: theme.spacing.unit * 2,
    background: 'none',
    borderStyle: 'dashed',
    borderWidth: 1,
  }

});
class MotorController extends Component {

  constructor(props) {
    super(props);
    this.state = {
      motor_list: [],
      settings: {},
      fetch_timestamp: undefined,
      loading: true,
      editMode: true,
      live: true,
    };
    this.oscPort = 3000;
    this.ip = window.location.hostname;
    this.APIport = 5000;
    this.APIendpoint = 'http://' + this.ip + ":" + this.APIport + "/api/";

    axios.defaults.baseURL = this.APIendpoint;
    axios.defaults.headers['Content-Type'] = 'application/json';
  }

  componentDidMount() {
    this.fetchSettings()
    this.fetchMotors()

    this.timerID = setInterval(
      () => this.liveFetch(),
      50
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  handleMotorListChange = (list) => {
    //console.log("new", list.slice())
    let diff = _.difference(_.map(this.state.motor_list, "id"), _.map(list, "id"));
    let del_list = _.filter(this.state.motor_list, function(obj) { 
      return diff.indexOf(obj.id) >= 0; })

    del_list = del_list.map(obj => { obj.deleted = true; return obj;} )
    // TODO: clean deleted elements from list after timeout

    const new_list = list.concat(del_list)

    this.setState({
      motor_list: new_list,
      loading: false,
      fetch_timestamp: new Date()
    })
  }
  
  addMotor = async (e) => {
    try {
      const resp = await axios.post("motors", {});
      console.log('Motor added:', resp.data);
      let new_motor_list = this.state.motor_list;
      new_motor_list.push(resp.data);
      this.setState({
        motor_list: new_motor_list,
      });
    }
    catch (error) {
      console.error('addMotor error:', error);
      return Promise.reject(error);
    }
  }

  deleteMotor = async (motor) => {
    try {
      const resp = await axios.delete("motors/" + motor.id);
      console.log(resp)
      return (resp.status === 204)
    }
    catch (error) {
      console.error('deleteMotor error:', error);
      return Promise.reject(error);
    }
  }

  getMotor = async (motor) => {
    try {
      const resp = await axios.get("motors/" + motor.id)
      return resp.data
    }
    catch (error) {
      console.error('getMotor error:', error);
      return Promise.reject(error)
    }
  }

  toggleEditMode = () => (e) => {
    this.setState({editMode: e.target.checked });
  }

  updateState = async (state) => {

    console.log(state)
    try {
      const response = await axios.put(this.APIendpoint + "state", state)

      this.setState({
        motor_list: response.data.motors,
        settings: response.data.settings
      })
      console.log(this)

    } catch(error) {
      console.error('Error:', error);
      return Promise.reject(error);
    }
  }

  updateMotorConfig = async (motor, obj) => {
    try {
      return await axios.put("motors/" + motor.id, obj)     
    } catch(error) {
      console.error('Error:', error);
      return Promise.reject(error);
    } 
  }

  fetchSettings = async () => {
    try {
      const response = await axios.get("settings")
      this.setState({
        settings: response.data
      })
      return response

    } catch(error) {
      console.error('Error:', error)
      return Promise.reject(error)
    }
  }

  updateSettings = async (settings) => {
    try {
      const response = await axios.put("settings", settings)
      this.setState({
        settings: response.data
      })
      return response;
    } catch(error) {
      console.error('Error:', error);
      return Promise.reject(error);
    }
  }

  updateMotorValue = (motor, _value) => {
    axios.put("motors/" + motor.id+ "/value", {"v": _value})
    .then(response => console.log('Success:', JSON.stringify(response)))
    .catch(error => console.error('Error:', error));
  }

  liveFetch = () => {
    if(this.state.live) {
      this.fetchMotors()
    }
  }

  fetchMotors = async () => {
    try {
      const res = await axios.get("motors");
      this.handleMotorListChange(res.data);
    }
    catch (error) {
      console.error('Error:', error);
      return Promise.reject(error);
    }
  }

  toggleLive = (e) => {
    this.setState({live: e.target.checked})
  }

  exportStateToFile = async () => {
    try {
      const response = await axios.get("state", {responseType: 'blob'});

      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'motor_controller_settings.json');
      document.body.appendChild(link);
      link.click();

    }
    catch (error) {
      console.error('handleExportToFile error:', error);
      return Promise.reject(error);
    }
  }

  renderMotors(motor_list) {
    const { classes } = this.props;

      const motors = motor_list;
      if(this.state.loading) {
        return (
          <CircularProgress className={classes.progress} />
        );
      }
      
      const listMotors = motors.map((motor, key) =>
            <Motor key={motor.id} motor={motor} editMode={this.state.editMode} API={{delete:this.deleteMotor, updateConfig: this.updateMotorConfig, updateValue: this.updateMotorValue}}/>
      );

      return (
        <Grid container spacing={8}>
          {listMotors}
          
          <Fade in={this.state.editMode}>
          <Grid item xs={6} sm={3} md={2} xl={1}>
          <Paper className={classes.add_container}>

          <Tooltip title="Add motor" placement="bottom">
              <Fab size="large" color="primary" aria-label="Add" className={classes.add_button} onClick={this.addMotor}>
                <AddIcon />
              </Fab>
          </Tooltip>
          </Paper>

          </Grid>
          </Fade>

        </Grid>
      );
  }

  render() {
    const { classes } = this.props;

    return(
      <MuiThemeProvider theme={theme}>
      <div className={classes.root}>

      <div className={classes.motor_list}>
        {this.renderMotors(this.state.motor_list) }
      </div>
      
        <AppBar position="fixed" color="default" className={classes.appBar}>
          <Toolbar className={classes.toolbar}>
            
            <Typography variant="h6" className={classes.title}>
              Flyvende Grises Servo Motor Styring  
            </Typography>

            {/*<Typography variant="body1" color="inherit">
              {this.APIendpoint}
              {this.state.settings.artnet_ip}
    </Typography>*/}
            <FormControlLabel
              control={<Switch size="small" checked={this.state.live} onChange={this.toggleLive} />}
              label="Live"
              name="live"
              />

            <SettingsDialog settings={this.state.settings} 
              exportStateToFile={ this.exportStateToFile }
              updateState={ this.updateState }
              updateSettings={ this.updateSettings }
              fetchSettings={ this.fetchSettings } />

          </Toolbar>
        </AppBar>

      </div>
      </MuiThemeProvider>
    );
  }
}

MotorController.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MotorController);
