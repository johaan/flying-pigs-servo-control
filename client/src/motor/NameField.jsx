import React, { Component } from 'react'
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';

import FormLabel from '@material-ui/core/FormLabel';


const styles = theme => ({
  textField: {
    width: '100%',
  },
});

class MotorNameField extends Component {

  constructor(props) {
    super(props);
    this.state = {value: props.value, sync: false};
  }

  handleChange = e =>  {
    const name = e.target.value;

    this.setState({
      value: name.replace(/[\W]+/g,"")
    });

  }

  onKeyPress = e =>  {
    console.log(e)
    if (e.key === 'Enter') {
      e.preventDefault();
      this.submit() 
    }
  }

  onBlur = e =>  {
    console.log(e)
    e.preventDefault();
    this.submit() 
  }

  submit = () => {
    if(this.state.value.length > 0) {
      if(this.props.value !== this.state.value) {
        this.props.handleNameChange(this.state.value);
      }
    } else {
      this.setState({
        value: this.props.value
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      this.setState({
        value: this.props.value
      });
    }
  }

  render() {
    const { classes } = this.props;
    const value = this.state.value;

    return (      
          <TextField
            id="motor-name"
            value={value}
            className={classes.textField}
            onChange={this.handleChange}
            onKeyPress={this.onKeyPress}
            onBlur={this.onBlur}
            helperText={"OSC: /motor/" + value }
            label="Address"
            margin="dense"
          />
    );
  }
}

MotorNameField.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MotorNameField);
