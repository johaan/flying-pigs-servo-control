import React, { Component } from 'react'
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';

// TODO: use a selector instead - handle what channels are possible to choose
const styles = theme => ({
  textField: {
    width: '100%',
  },
});

class ChannelField extends Component {

  constructor(props) {
    super(props);
    this.state = {value: props.value};
  }

  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      this.setState({
        value: this.props.value,
      });
    }
  }

  handleChange = e =>  {
    const channel = e.target.value;
    if((parseInt(channel) && channel > 0 && channel < 513) || channel === '') {
      this.setState({
        value: channel
      }); 
    } else {
      console.error("Channel has to be a number between 1 and 512")
    }
  }

  submit = () => {
    const v = this.state.value
    this.props.handleChannelChange(v);
  }

  onKeyPress = e =>  {
    if (e.key === 'Enter') {
      e.preventDefault();
      this.submit() 
    }
  }
  
  onBlur = e =>  {
    console.log(e)
    e.preventDefault();
    this.submit() 
  }

  render() {
    const { classes } = this.props;
    const value = this.state.value;

    return (      
        <TextField
          id="motor-channel"
          value={value}
          type="number"
          className={classes.textField}
          onChange={this.handleChange}
          onKeyPress={this.onKeyPress}
          onBlur={this.onBlur}
          label="Channel"
          helperText="DMX Address 1-512"
          margin="dense"
        />
    );
  }
}

ChannelField.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ChannelField);
