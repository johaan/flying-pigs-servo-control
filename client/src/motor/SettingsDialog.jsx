import React, { Component } from 'react'

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import IconButton from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';


const styles = theme => ({
  container: {
  }
});

class SettingsDialog extends Component{

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      settings: {}
    };

  }

  handleClickOpen = async () => {

    const response = await this.props.fetchSettings()
    console.log('handleClickOpen', response.data)

    this.setState({
      settings: response.data,
      open: true
    });
    
  }

  handleChange = (e) => {
    let settings = this.state.settings;

    if(e.target.type === 'checkbox') {
      settings[e.target.name] = e.target.checked
    } else {
      settings[e.target.name] = e.target.value
    }

    this.setState({settings: settings})
  }


  handleClose = () => {
    this.setState({open: false});
  }

  handleSave = async () => {

    // submit data from state
    const response = await this.props.updateSettings(this.state.settings)
    // on success:
    this.setState({settings: response.data, open: false});

  }

  handleExportToFile = (e) => {
    this.props.exportStateToFile()
  }

  handleLoadFromFile = (e) => {
    const file = e.target.files[0]

    const onReaderLoad = (event) => {
      var obj = JSON.parse(event.target.result);
      this.props.updateState(obj)
    }

    var reader = new FileReader();
        reader.onload = onReaderLoad;
        reader.readAsText(file);
  }


  render() {
    const { classes } = this.props;
    const { settings } = this.state;

    return (
      <div>

        <IconButton className={classes.button} aria-label="settings" onClick={this.handleClickOpen}>
        <SettingsIcon />
      </IconButton>
        

        <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Settings</DialogTitle>
          <DialogContent>
            {/*<DialogContentText>
              Saving may force a restart of the server.
            </DialogContentText>*/}
            <TextField
              value={settings.osc_forward_host}
              onChange={this.handleChange}
              margin="dense"
              name="osc_forward_host"
              label="OSC Forward host"
              type="text"
              fullWidth
            />
            <TextField
              value={settings.osc_forward_port}
              onChange={this.handleChange}
              margin="dense"
              name="osc_forward_port"
              label="OSC Forward port"
              type="number"
              fullWidth
            />

            <FormControlLabel
            control={<Switch size="small" checked={settings.osc_forward_enabled} onChange={this.handleChange} />}
            label="OSC Forward"
            name="osc_forward_enabled"
            />
            
            <TextField
              value={settings.osc_listen_port}
              onChange={this.handleChange}
              margin="dense"
              name="osc_listen_port"
              label="OSC Listen port"
              type="number"
              fullWidth
            />

            <TextField
              value={settings.artnet_ip}
              onChange={this.handleChange}
              margin="dense"
              name="artnet_ip"
              label="ArtNet IP"
              type="text"
              fullWidth
            />

          </DialogContent>
          <DialogActions>

            <Button className={classes.button} onClick={this.handleClose}>
              Cancel
            </Button>

            <input
              accept="json/*" 
              ref={'file-upload'}
              type='file'
              id="json-file-input"
              style={{visibility: 'hidden', display: 'none'}}
              onChange={this.handleLoadFromFile}
            />
            <label htmlFor="json-file-input"> 
            <Button className={classes.button} type='file' onClick={e => {
              this.refs['file-upload'].click()
            }}>
              Load
            </Button>
            </label>

            <Button className={classes.button} onClick={this.handleExportToFile}>
              Export
            </Button>

            <Button className={classes.button} onClick={this.handleSave}>
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(SettingsDialog);