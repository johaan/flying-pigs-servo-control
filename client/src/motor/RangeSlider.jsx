import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Slider, Rail, Handles, Tracks, Ticks } from 'react-compound-slider'
import { withStyles } from '@material-ui/core/styles'
import { SliderRail, Handle, Track, Tick } from './slider_components' // example render components - source below
import _ from 'lodash'

const style = () => ({
  root: {
    width: 40,
    height: 250,
  },
  slider: {
    position: 'relative',
    height: '100%',
  },
})

const domain = [0, 1]

class MotorRangeSlider extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userSlideInProgress: false,
      values: props.values
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.values !== state.values) {
      return {values: props.values, changedFromProps: true}
    } 
    return null
  }

  onSlideStart = () => {
    this.setState({userSlideInProgress: true})
  }

  onSlideEnd = () => {
    this.setState({userSlideInProgress: false})
  }

  onUpdate = values => {
    if(this.state.changedFromProps) {
      this.setState({changedFromProps: false})
    } else {
      this.props.handleSliderChange(values)
    }
  }

  onChange = values => {
    //if(this.state.changedFromProps) {
    //  this.setState({changedFromProps: false})
    //} else {
      this.props.handleSliderChange(values)
    //}    
  }

  render() {
    const {
      props: { classes, disabled=false, reversed },
      state: { values },
    } = this

    return (
      <div className={classes.root}>
        <Slider
          vertical
          reversed={reversed}
          disabled={disabled}
          mode={2}
          step={0.001}
          domain={domain}
          className={classes.slider}
          onUpdate={_.throttle(this.onUpdate, 50)}
          onChange={this.onChange}
          onSlideStart={this.onSlideStart}
          onSlideEnd={this.onSlideEnd}
          values={values}
        >
          <Rail>
            {({ getRailProps }) => <SliderRail getRailProps={getRailProps} />}
          </Rail>
          <Handles>
            {({ activeHandleID, handles, getHandleProps }) => (
              <div>
                {handles.map(handle => (
                  <Handle
                    key={handle.id}
                    handle={handle}
                    domain={domain}
                    activeHandleID={activeHandleID}
                    getHandleProps={getHandleProps}
                    disabled={disabled}
                  />
                ))}
              </div>
            )}
          </Handles>
          <Tracks left={false} right={false}>
            {({ tracks, getTrackProps }) => (
              <div>
                {tracks.map(({ id, source, target }) => (
                  <Track
                    key={id}
                    source={source}
                    target={target}
                    getTrackProps={getTrackProps}
                    disabled={disabled}
                  />
                ))}
              </div>
            )}
          </Tracks>
        </Slider>
      </div>
    )
  }
}

MotorRangeSlider.propTypes = {
  classes: PropTypes.object.isRequired,
}


export default withStyles(style)(MotorRangeSlider)
