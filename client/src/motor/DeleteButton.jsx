import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import DeleteIcon from '@material-ui/icons/RemoveCircle';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';

const styles = theme => ({
  button: {
    position: 'absolute',
    top: theme.spacing.unit * 0.5,
    right: theme.spacing.unit * 0.5,
  }
});

class MotorDeleteButton extends Component {

  handleClick = e =>  {
    this.props.handleDelete(e);
  }

  render() {
    const { classes } = this.props;
    return (
      <Tooltip title="Delete" placement="right">
      <IconButton size="small" aria-label="Delete" className={classes.button} onClick={this.handleClick}>
        <DeleteIcon fontSize="small" color="secondary"/>
      </IconButton>
      </Tooltip>
    );
  }

}

MotorDeleteButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MotorDeleteButton);
