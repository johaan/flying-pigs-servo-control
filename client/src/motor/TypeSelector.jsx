import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Tooltip from '@material-ui/core/Tooltip';

// TODO: get type enum from API

const styles = theme => ({
});

class MotorTypeSelector extends Component {

  constructor(props) {
    super(props);
    this.state = {value: props.value, fetching: false};
  }

  componentDidMount() {
  }

  handleChange = e =>  {
    const val = e.target.value;
    this.setState({
      value: val,
      fetching: true
    });
    this.props.handleTypeChange(val);
  }

  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      this.setState({
        value: this.props.value
      });
    }
  }

  render() {
    const { classes } = this.props;
    const value = this.state.value;

    return (
        <Tooltip title="Motortype" placement="right">

          <Select
            value={value}
            onChange={this.handleChange}
            inputProps={{
              name: 'type',
              id: 'type-select',
            }}
            
          >
            <MenuItem value={1}>SERVO</MenuItem>
            <MenuItem value={2}>DYNAMIXEL</MenuItem>
            <MenuItem value={3}>CONTINUOUS</MenuItem>
            <MenuItem value={4}>ENCODED</MenuItem>

          </Select>
          </Tooltip>
    );
  }
}

MotorTypeSelector.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MotorTypeSelector);


